import { TypeNotification } from "@/interfaces/INotification";
import { store } from '@/store'
import { NOTIFY } from "@/store/types-mutations";

type Notify = {
    notify: (type: TypeNotification, title: string, description: string) => void
}

export default (): Notify => {
    const notify = (type: TypeNotification, title: string, description: string): void => {
        store.commit(NOTIFY, {
            title,
            description,
            type
        })
    }

    return {
        notify
    }
}