export enum TypeNotification {
    SUCCESS,
    FAIL,
    WARNING
}
export interface INotification {
    title: string
    description: string
    type: TypeNotification
    id: number
}