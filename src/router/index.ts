import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Tasks from '../views/Tasks.vue'
import Projects from '../views/Projects.vue'
import Form from '../views/Projetos/Form.vue'
import List from '../views/Projetos/List.vue'

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Home',
        component: Tasks
    },
    {
        path: '/projetos',
        component: Projects,
        children: [
            {
                path: '',
                name: 'Projetos',
                component: List
            },
            {
                path: 'novo',
                name: 'Novo Projeto',
                component: Form
            },
            {
                path: ':id',
                name: 'Editar Projeto',
                component: Form,
                props: true
            }
        ]
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

export default router;