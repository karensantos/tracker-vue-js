import { INotification } from "@/interfaces/INotification";
import IProject from "@/interfaces/IProject";
import ITask from "@/interfaces/ITasks";
import { InjectionKey } from "vue";
import { createStore, Store, useStore as vuexUseStore } from "vuex";
import { ADD_PROJECT, EDIT_PROJECT, DELETE_PROJECT, ADD_TASK, DELETE_TASK, EDIT_TASK, NOTIFY } from "./types-mutations";

interface StateApp {
    projects: IProject[],
    tasks: ITask[],
    notifications: INotification [] 
}

export const key: InjectionKey<Store<StateApp>> = Symbol()

export const store = createStore<StateApp>({
    state: {
        projects: [],
        tasks: [],
        notifications: []
    },
    mutations: {
        // mutations PROJECTS
        [ADD_PROJECT](state, name: string) {
            const project = {
                id: new Date().toISOString(),
                name
            } as IProject

            state.projects.push(project)
        },
        [EDIT_PROJECT](state, project: IProject) {
            const index = state.projects.findIndex(proj => proj.id == project.id)
            state.projects[index] = project
        },
        [DELETE_PROJECT](state, id: string) {
            state.projects = state.projects.filter(proj => proj.id != id)
        },
        // mutations TASKS
        [ADD_TASK] (state, task: ITask) {
            task.id = new Date().toISOString()
            state.tasks.push(task)
        },
        [EDIT_TASK](state, task: ITask) {
            const indice = state.tasks.findIndex(p => p.id == task.id)
            state.tasks[indice] = task
        },
        [DELETE_TASK](state, id: string) {
            state.projects = state.projects.filter(p => p.id != id)
        },
        // mutations NOTIFICATIONS
        [NOTIFY](state, newNotification: INotification) {
            newNotification.id = new Date().getTime()
            state.notifications.push(newNotification)
            setTimeout(()=>{
                state.notifications = state.notifications.filter(notification => notification.id != newNotification.id)
            }, 3000)
        }
    }
})

export function useStore(): Store<StateApp> {
    return vuexUseStore(key)
}