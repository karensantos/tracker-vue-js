import { TypeNotification } from "@/interfaces/INotification"
import { NOTIFY } from "@/store/types-mutations"
import { store } from '@/store'

export const notifyMixin = {
    methods: {
        notify(type: TypeNotification, title: string, description: string) {
            store.commit(NOTIFY, {
                title,
                description,
                type
            })
        }
    },
}